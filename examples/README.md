# Python Extension Examples

This directory contains examples of what Python extensions might look like. Each extension is in its
own directory, where you can inspect its source code.


## How to install

Simply run `make copyexamples` to copy the examples to the right location
in the build directory.
