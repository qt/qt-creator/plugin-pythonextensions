TEMPLATE = subdirs

SUBDIRS += \
    pythonextensions \
    python \
    optional \
    examples

pythonextensions.subdir = plugins/pythonextensions

optional.depends = pythonextensions

copyexamples.CONFIG += recursive
copyexamples.recurse = examples
QMAKE_EXTRA_TARGETS += copyexamples
