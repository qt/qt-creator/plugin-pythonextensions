# Optional Bindings

This directory contains all optional bindings for Qt Creator
Modules that are bundled with this plugin.

Optional bindings come in the form of dynamically loaded libraries,
each binding has its own folder. The template directory contains files
that are shared for each binding.

## Documentation

### Writing optional binding libraries
**NOTICE:** Please refer to the `pyutil.h` header file for anything that is not explained
here.

Each library project here must include a `binding.cpp` file which must implement
a function with the signature `void bind();`.
