
# Declare dependencies and name

# This has to be PythonBinding{PluginName}
QTC_PLUGIN_NAME = PythonBindingProjectExplorer
QTC_LIB_DEPENDS += \
    extensionsystem \
    utils

QTC_PLUGIN_DEPENDS += \
    coreplugin \
    projectexplorer \
    pythonextensions

QTC_PLUGIN_RECOMMENDS += \
    # optional plugin dependencies. nothing here at this time

include(../binding/binding.pri)

INCLUDEPATH *= $$IDE_SOURCE_TREE/src/plugins/projectexplorer

win32: DEFINES += NOMINMAX

# Shiboken binding generation setup

WRAPPED_HEADER = $$PWD/wrappedclasses.h
WRAPPER_DIR = $$OUT_PWD/QtCreatorBindingProjectExplorer
TYPESYSTEM_FILE = typesystem_projectexplorer.xml
TYPESYSTEM_NAME = qtcreatorbindingprojectexplorer
SHIBOKEN_QT = core gui widgets network

## These headers are needed so the generated wrappers are added to the
## build. Right now they are empty files, however there might be a more elegant
## option.
WRAPPED_CLASSES = \
  bindingheaders/projectexplorer.h \
  bindingheaders/projectexplorer_buildconfiguration.h \
  bindingheaders/projectexplorer_projectconfiguration.h \
  bindingheaders/projectexplorer_statefulprojectconfiguration.h \
  # bindingheaders/projectexplorer_ibuildconfigurationfactory.h \
  bindingheaders/projectexplorer_runconfiguration.h \
  bindingheaders/projectexplorer_project.h \
  bindingheaders/projectexplorer_projecttree.h \
# Sentinel line

include(../../shiboken.pri)
