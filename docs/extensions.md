# Documentation for Python Extension Authors

This document is intended to provide an introduction to writing Python extensions
for Qt Creator.

## Importing Qt Creator specific modules
Currently, the bindings are available from Python under the following names:
```Python
from QtCreator import Utils # Imports Qt Creator's Utils helper library
from QtCreator import Core # Imports Qt Creator's Core plugin API
from QtCreator import PythonExtensions # Imports Python extension specific helper functions
```

## `QtCreator.PythonExtensions`
This is the Python binding for the extension manager that works on the C++ side. It provides the
following functions, that can be used from Python:

```Python
from QtCreator import PythonExtensions

# Returns a PySide QDir which points to the extension directory
PythonExtensions.extensionDir()

# Returns a list with the names of all Python extensions
# Each extension has properties 'name' and 'loaded'
PythonExtensions.extensionList()

# Returns the path of the custom location to
# where missing dependencies should be pip installed
PythonExtensions.pythonPackagePath()
```


# Qt Creator bindings
Generally, the parts of Qt Creator that are exposed have an interface that is nearly
identical to the C++ interface.

## Working with Menus
You can add new items to the menus of Qt Creator. You can either add an action directly, or
add a new action container, that holds a sub-menu.

### Adding a sub menu
The following code snippet illustrates how to add a new menu.

```Python
from QtCreator import Core

def hello():
    print("Hello World.")

# By convention, the menuId starts with "Python"
menuId = "Python.SmallMenu.Menu"

menu = Core.ActionManager.createMenu(menuId)
menu.menu().setTitle("My menu")
menu.menu().addAction("My action", hello)
# Add our new menu to the "Tools" menu in Qt Creator
Core.ActionManager.actionContainer("QtCreator.Menu.Tools").addMenu(menu)
```

### Adding a new action directly
The following code snippet illustrates how to add a new action to an existing action container.

```Python
from QtCreator import Core

def hello():
    print("Hello World.")

# Add a new action to the "Tools" menu
menu = Core.ActionManager.actionContainer("QtCreator.Menu.Tools")
menu.menu().addAction("My action", hello)
```

### Menu Id list
Currently, the following menu ids are available in Qt Creator.

```Python
"QtCreator.Menu.File"
"QtCreator.Menu.File.RecentFiles"
"QtCreator.Menu.Edit"
"QtCreator.Menu.Edit.Advanced"
"QtCreator.Menu.Tools"
"QtCreator.Menu.Tools.External"
"QtCreator.Menu.Window"
"QtCreator.Menu.Window.Panes"
"QtCreator.Menu.Window.ModeStyles"
"QtCreator.Menu.Window.Views"
"QtCreator.Menu.Help"
```


# The embedded Python interpreter

## Python modules and `sys.path`
When importing modules, the following important locations will be checked (in this order):

 1. The folder of the extension itself (files and folders in your extension)
 2. The system Python path entries (anything you `pip install`ed globally)
 3. Qt Creator's python extension directory
 4. The Qt Creator specific Python module directory
    - Note: This is where you should install any dependencies missing
      if you want to use non-standard Python packages / modules
    - This last path is accessible with `PluginInstance.pythonPackagePath()`

## Installing dependencies

If you want to install dependencies, you can include a standard Python `requirements.txt`
file in your extension folder. **Note that this file is pip installed _once_.** After the initial
pip install, a new file called `requirements.txt.installed` is created in the extensions folder. To
run the install again, this file simply has to be deleted. **Be careful to remove this file, when
distributing your extension in a .zip file.**

Otherwise you can do any setup you need in your extension's initialization code.
